package autotest.pages.actions;

import autotest.core.base.BasePage;
import autotest.core.business.User;
import autotest.pages.elements.SignInPage;
import org.openqa.selenium.Keys;

public class SignInPageHelper extends BasePage {
    private final SignInPage signInPage = new SignInPage();

    public SignInPageHelper() {
        super();
    }

    public String checkForInvalidInput(User user){
        waitForElementVisibility(signInPage.getEmailInput());
        signInPage.getEmailInput().sendKeys(user.getEmail()+ Keys.ENTER);


//        waitForElementToBeClickable(signInPage.getNextBtn());
//        signInPage.getNextBtn().click();

        sleep(2000);
        waitForElementVisibility(signInPage.getCannotSignInText());
        return signInPage.getCannotSignInText().getText();
    }

}

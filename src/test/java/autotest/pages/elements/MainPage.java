package autotest.pages.elements;


import autotest.core.annotations.DescriptionOfElement;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends CommonElements {

    @DescriptionOfElement("Поиск")
    @Getter
    @FindBy(xpath = "//input[@title='Search']")
    private WebElement searchInput;


    @DescriptionOfElement("кнопка - Google Search")
    @Getter
    @FindBy(name = "btnK")
    private WebElement googleSearchBtn;

}
